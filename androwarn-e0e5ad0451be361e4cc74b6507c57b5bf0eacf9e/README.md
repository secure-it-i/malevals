Download androwarn.tzr.gz to obtain the version of the tool that was used for evaluation

#Dependencies

1. Install python, jinja2, git and mercurial development packages on your host

2. Download the latest Chilkat module on https://www.chilkatsoft.com/python.asp, according to your architecture (32 or 64 bits) and your python version (2.5, 2.6, 2.7, 3.0, 3.1, 3.2)

#Usage

`$ python androwarn.py -i my_application_to_be_analyzed.apk -r html -v 3`

`python androwarn.py -h` to see full options

# References

[AndroWarn](https://github.com/maaaaz/androwarn)

[Installation](https://github.com/maaaaz/androwarn/wiki/Installation)
