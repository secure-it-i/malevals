This repository captures results of evaluating freely available tools that detect malicious behavior in Android apps.

The following tools were evaluated

1. **AndroTotal**
Online service that scans Android apps for suspicious behavior

2. **AndroWarn**
A static code analyzer for malicious Android applications

3. **Maldrolyzer**
A framework to extract "actionable" data from Android malware (C&Cs, phone numbers etc.)

4. **NVisioApkScan**
Online service that scans Android apps for suspicious behavior

4. **VirusTotal**
Online service that scans Android apps for suspicious behavior


Each tool was run against the *Malicious* app of each benchmark in [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) - an open-source repository of Android app vulnerability benchmarks. The results of each tool run is captured in a folder named after tool-version (if version info for the tool was available). The version is nothing but the commit id of the repository used for evaluation. Each folder also a compressed form of the tool that was used for evaluation. The output/default folder in each tool contains the reports of the evaluation.

# Contact

Please raise an *Issue* if you have any questions about the experiment or the data.
