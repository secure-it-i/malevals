Download maldrolyzer.tar.gz to obtain the version used for evaluation.

# Dependencies

Androguard

`$ git clone https://github.com/androguard/androguard`

`$ cd androguard`

`$ sudo python setup.py install`

PyCrypto

`$ easy_install pycrypto`

pyelftools

`$ easy_install pyelftools`

yara

`$ easy_install yara`

# Usage

`$ python maldrolyzer.py`

#References

[Maldrolyzer](https://github.com/maldroid/maldrolyzer)
